﻿// Patrol.cs
using UnityEngine;
using System.Collections;
/*
 * This code is free to use, there is no warranty of work, you are free to 
 * modify, distribute or use it in any way you want, giving or not mention of the original authors.
 * 
 * Original Author: Manuel Otheo
 * Last Modification: Manuel Otheo
 * 
 * SUMMARY: This is the the simple form possible of waypoint follow
 * 
 */

public class AIWaypointSimplePatrol : MonoBehaviour {

	[Tooltip("The array of patrolling points")]
	public Transform[] points;

	[Tooltip("To which point it is heading")]
	private int destPoint = 0;

	[Tooltip("The component NavMesh Agent of the patroller")]
	private NavMeshAgent agent;


	void Start () {
		agent = GetComponent<NavMeshAgent>();

		// Disabling auto-braking allows for continuous movement
		// between points (ie, the agent doesn't slow down as it
		// approaches a destination point).
		agent.autoBraking = false;

		GotoNextPoint();
	}


	void GotoNextPoint() {
		// Returns if no points have been set up
		if (points.Length == 0)
			return;

		// Set the agent to go to the currently selected destination.
		agent.destination = points[destPoint].position;

		// Choose the next point in the array as the destination,
		// cycling to the start if necessary.
		destPoint = (destPoint + 1) % points.Length;
	}


	void Update () {
		// Choose the next destination point when the agent gets
		// close to the current one.
		if (agent.remainingDistance < 0.5f)
			GotoNextPoint();
	}
}