﻿using UnityEngine;
using System.Collections;

/*
 * This code is free to use, there is no warranty of work, you are free to 
 * modify, distribute or use it in any way you want, giving or not mention of the original authors.
 * 
 * Original Author: Manuel Otheo
 * Last Modification: Daniel Alvarez
 * 
 * SUMMARY: This script is intended to give you some guidelines and ideas on how to write your code, 
 * you are free to use the guidelines or not use them at all.
 * 
 */


public class Template : MonoBehaviour {


	/// <summary>
	/// This is an example of a variable with a tooltip on the editor.
	/// </summary>
	[Tooltip("An example of a tooltip")]
	public int hoverForTooltip = 0;


	/// <summary>
	/// This type of comments appear in the IDE when you try to writhe a method, so they are nice to explain your code
	/// </summary>
	/// <param name="exampleVariable">This is an example of how a variable is shown with the tooltip.</param>
	public void ExampleFunction(int exampleVariable){
	}




	// You should try to write ExampleFunction to see the tooltip
	// ExampleFunction( should do the trick
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
